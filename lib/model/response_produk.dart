// To parse this JSON data, do
//
//     final responseProduk = responseProdukFromJson(jsonString);

import 'dart:convert';

List<ResponseProduk> responseProdukFromJson(String str) =>
    List<ResponseProduk>.from(
        json.decode(str).map((x) => ResponseProduk.fromJson(x)));

String responseProdukToJson(List<ResponseProduk> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ResponseProduk {
  ResponseProduk({
    this.foodCode,
    required this.name,
    required this.picture,
    required this.pictureOri,
    required this.createdAt,
    this.price,
    required this.id,
  });

  String? foodCode;
  String name;
  String picture;
  String? price;
  String pictureOri;
  DateTime createdAt;
  int id;

  factory ResponseProduk.fromJson(Map<String, dynamic> json) => ResponseProduk(
        foodCode: json["food_code"],
        name: json["name"],
        picture: json["picture"],
        price: json["price"] ?? '',
        pictureOri: json["picture_ori"],
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "food_code": foodCode,
        "name": name,
        "picture": picture,
        "picture_ori": pictureOri,
        "created_at": createdAt.toIso8601String(),
        "id": id,
      };
}
