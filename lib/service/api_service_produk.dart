import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:reza_mobileappps/constant.dart';

class ApiServiceProduk {
  Future<http.Response> getData() async {
    var response = await http.get(
      Uri.parse(Constant.baseUrl),
      headers: {
        "Content-Type": "application/json",
        "X-SECRET-TOKEN": Constant.secretToken
      },
    );
    return response;
  }

  Future<http.Response> createData({
    String? foodCode,
    String? name,
    String? pictureOri,
    String? price,
    String? pictureLink,
  }) async {
    var response = await http.post(Uri.parse(Constant.baseUrl),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "X-SECRET-TOKEN": Constant.secretToken
        },
        body: jsonEncode({
          "food_code": foodCode,
          "name": name,
          "picture": pictureLink,
          "picture_ori": pictureOri,
          "created_at": DateTime.now().toLocal().toString(),
          "price": price
        }));
    return response;
  }

  Future<http.Response> editData(
      {String? foodCode,
      String? name,
      String? pictureOri,
      String? price,
      String? pictureLink,
      required int id}) async {
    final url = Uri.encodeFull(Constant.baseUrl + id.toString());
    var response = await http.patch(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "X-SECRET-TOKEN": Constant.secretToken
        },
        body: jsonEncode({
          "food_code": foodCode,
          "name": name,
          "picture": pictureLink,
          "picture_ori": pictureOri,
          "created_at": DateTime.now().toLocal().toString(),
          "price": price
        }));
    return response;
  }

  Future<http.Response> deleteData({required int id}) async {
    final url = Uri.encodeFull(Constant.baseUrl + id.toString());
    var response = await http.delete(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "X-SECRET-TOKEN": Constant.secretToken
      },
    );
    return response;
  }
}
