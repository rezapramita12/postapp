part of 'produk_bloc.dart';

abstract class ProdukState extends Equatable {
  const ProdukState();

  @override
  List<Object> get props => [];
}

class ProdukInitial extends ProdukState {}

class GetProdukLoading extends ProdukState {}

class GetProdukSuccess extends ProdukState {
  final List<ResponseProduk> data;
  const GetProdukSuccess(this.data);
}

class GetProdukError extends ProdukState {
  final String error;
  const GetProdukError(this.error);
}
