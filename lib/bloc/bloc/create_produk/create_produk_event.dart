part of 'create_produk_bloc.dart';

abstract class CreateProdukEvent extends Equatable {
  const CreateProdukEvent();

  @override
  List<Object> get props => [];
}

class AddProdukEvent extends CreateProdukEvent {
  final String? foodCode;
  final String? name;
  final String? price;
  final String? pictureLink;
  final String? pictureOriLink;

  const AddProdukEvent(
      {required this.foodCode,
      required this.name,
      required this.price,
      required this.pictureLink,
      required this.pictureOriLink});
}

class UpdateProdukEvent extends CreateProdukEvent {
  final String? foodCode;
  final String? name;
  final String? price;
  final String? pictureLink;
  final String? pictureOriLink;
  final int id;

  const UpdateProdukEvent(
      {required this.foodCode,
      required this.name,
      required this.id,
      required this.price,
      required this.pictureLink,
      required this.pictureOriLink});
}

class DeleteProdukEvent extends CreateProdukEvent {
  final int id;
  const DeleteProdukEvent(this.id);
}
