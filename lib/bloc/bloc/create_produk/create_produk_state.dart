part of 'create_produk_bloc.dart';

abstract class CreateProdukState extends Equatable {
  const CreateProdukState();

  @override
  List<Object> get props => [];
}

class CreateProdukInitial extends CreateProdukState {}

class CreateProdukLoading extends CreateProdukState {}

class CreateProdukSuccess extends CreateProdukState {
  const CreateProdukSuccess();
}

class CreateProdukError extends CreateProdukState {
  final String error;
  const CreateProdukError(this.error);
}

class EditProdukLoading extends CreateProdukState {}

class EditProdukSuccess extends CreateProdukState {}

class EditProdukError extends CreateProdukState {
  final String error;
  const EditProdukError(this.error);
}

class DeleteProdukLoading extends CreateProdukState {}

class DeleteProdukSuccess extends CreateProdukState {}

class DeleteProdukError extends CreateProdukState {
  final String error;
  const DeleteProdukError(this.error);
}
