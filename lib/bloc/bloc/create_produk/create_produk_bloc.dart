import 'dart:convert';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:reza_mobileappps/service/api_service_produk.dart';

part 'create_produk_event.dart';
part 'create_produk_state.dart';

class CreateProdukBloc extends Bloc<CreateProdukEvent, CreateProdukState> {
  final ApiServiceProduk _apiServiceProduk = ApiServiceProduk();

  CreateProdukBloc() : super(CreateProdukInitial()) {
    on<AddProdukEvent>((event, emit) async {
      emit(CreateProdukLoading());
      try {
        log('masuk sini');

        final response = await _apiServiceProduk.createData(
            foodCode: event.foodCode,
            price: event.price,
            name: event.name,
            pictureLink: event.pictureLink,
            pictureOri: event.pictureOriLink);
        if (response.statusCode == 201) {
          log('masuk success');
          emit(const CreateProdukSuccess());
        } else {
          emit(CreateProdukError(jsonDecode(response.body)));
          log('masuk error${response.statusCode}');
        }
      } catch (e) {
        emit(CreateProdukError(e.toString()));
        log(e.toString());
      }
      // TODO: implement event handler
    });
    on<UpdateProdukEvent>((event, emit) async {
      emit(EditProdukLoading());
      try {
        log('masuk sini');

        final response = await _apiServiceProduk.editData(
            id: event.id,
            foodCode: event.foodCode,
            price: event.price,
            name: event.name,
            pictureLink: event.pictureLink,
            pictureOri: event.pictureOriLink);
        if (response.statusCode == 200) {
          emit(EditProdukSuccess());
        } else {
          emit(EditProdukError(jsonDecode(response.body)));
        }
      } catch (e) {
        emit(EditProdukError(e.toString()));
      }
      // TODO: implement event handler
    });
    on<DeleteProdukEvent>((event, emit) async {
      emit(DeleteProdukLoading());
      try {
        log('masuk sini');

        final response = await _apiServiceProduk.deleteData(
          id: event.id,
        );
        if (response.statusCode == 200) {
          emit(DeleteProdukSuccess());
        } else {
          emit(DeleteProdukError(jsonDecode(response.body)));
        }
      } catch (e) {
        emit(DeleteProdukError(e.toString()));
      }
      // TODO: implement event handler
    });
  }
}
