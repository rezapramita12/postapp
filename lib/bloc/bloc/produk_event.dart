part of 'produk_bloc.dart';

abstract class ProdukEvent extends Equatable {
  const ProdukEvent();

  @override
  List<Object> get props => [];
}

class GetProdukEvent extends ProdukEvent {}
