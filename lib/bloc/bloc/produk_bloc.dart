import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:reza_mobileappps/model/response_produk.dart';
import 'package:reza_mobileappps/service/api_service_produk.dart';

part 'produk_event.dart';
part 'produk_state.dart';

class ProdukBloc extends Bloc<ProdukEvent, ProdukState> {
  final ApiServiceProduk _apiServiceProduk = ApiServiceProduk();
  ProdukBloc() : super(ProdukInitial()) {
    on<GetProdukEvent>((event, emit) async {
      emit(GetProdukLoading());
      try {
        final response = await _apiServiceProduk.getData();
        if (response.statusCode == 200) {
          emit(GetProdukSuccess(responseProdukFromJson(response.body)));
        } else {
          emit(GetProdukError(jsonDecode(response.body)));
        }
      } catch (e) {
        emit(GetProdukError(e.toString()));
      }
      // TODO: implement event handler
    });
  }
}
