// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reza_mobileappps/bloc/bloc/produk_bloc.dart';
import 'package:reza_mobileappps/model/response_produk.dart';
import 'package:reza_mobileappps/ui/create_produk.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ProdukBloc _produkBloc = ProdukBloc();
  List<Cart> cart = [];
  final TextEditingController _controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool showCart = false;

  int kembalian = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _produkBloc.add(GetProdukEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const CreateProdukPage();
                }));
              },
              icon: const Icon(Icons.add))
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              BlocConsumer<ProdukBloc, ProdukState>(
                bloc: _produkBloc,
                listener: (context, state) {
                  // TODO: implement listener
                },
                builder: (context, state) {
                  if (state is GetProdukLoading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is GetProdukSuccess) {
                    return GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 20,
                          crossAxisSpacing: 6,
                          childAspectRatio: MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 1.3)),
                      itemCount: state.data.length,
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        ResponseProduk data = state.data[index];
                        return Card(
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  height: 150,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.2),
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16)),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16)),
                                    child: CachedNetworkImage(
                                      imageUrl: data.picture,
                                      placeholder: (context, url) =>
                                          const Center(
                                              child:
                                                  CircularProgressIndicator()),
                                      // errorWidget: (context, url, error) =>
                                      //     SizedBox(
                                      //   height: 200,
                                      //   child: Image.asset(
                                      //     'assets/postingan.png',
                                      //     fit: BoxFit.cover,
                                      //     width: double.maxFinite,
                                      //   ),
                                      // ),
                                      fit: BoxFit.cover,
                                      width: double.maxFinite,
                                    ),
                                  )),
                              const SizedBox(
                                height: 16.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(
                                  data.name,
                                  style: const TextStyle(fontSize: 12),
                                ),
                              ),
                              const SizedBox(
                                height: 3.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(
                                  "Rp.${data.price}/porsi",
                                  style: const TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue),
                                ),
                              ),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                width: double.infinity,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: ElevatedButton(
                                    onPressed: () {
                                      var dataCart = cart.where((element) =>
                                          element.data.name
                                              .contains(data.name));
                                      if (dataCart.isNotEmpty) {
                                        int index = cart.indexWhere((element) =>
                                            element.data.name == data.name);
                                        // cart[index].data = data;
                                        cart[index].qty = cart[index].qty! + 1;
                                      } else {
                                        cart.add(Cart(data: data, qty: 1));
                                      }
                                      setState(() {});
                                    },
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.blue[900]),
                                    child: const Text('Order')),
                              )
                            ],
                          ),
                        );
                      },
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
              width: 25,
              height: 25,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.blue[900], shape: BoxShape.circle),
              child: Center(
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      showCart = !showCart;
                    });
                  },
                  child: const Icon(
                    Icons.arrow_drop_up,
                    color: Colors.white,
                  ),
                ),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  children: [
                    const Icon(Icons.card_giftcard),
                    const SizedBox(
                      width: 10.0,
                    ),
                    cart.isNotEmpty
                        ? Text(
                            'Rp.${cart.fold(0, (previousValue, element) => previousValue + (int.parse(element.data.price ?? '0') * element.qty!))}')
                        : const Text('Rp.0')
                  ],
                ),
              ),
              Container(
                width: 150,
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ElevatedButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return StatefulBuilder(
                                builder: (context, setState) {
                              return WillPopScope(
                                onWillPop: () async {
                                  kembalian = 0;
                                  _controller.clear();
                                  setState(() {});
                                  Navigator.pop(context);
                                  return true;
                                },
                                child: AlertDialog(
                                  title: Center(
                                      child: Text(
                                    'Detail Pesanan',
                                    style: TextStyle(color: Colors.blue[900]),
                                  )),
                                  content: SingleChildScrollView(
                                    child: Form(
                                      key: _formKey,
                                      child: Column(
                                        children: [
                                          Column(
                                            children: cart.map((e) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 5.0,
                                                        vertical: 16),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Container(
                                                          width: 60,
                                                          height: 60,
                                                          decoration: BoxDecoration(
                                                              image: DecorationImage(
                                                                  image: NetworkImage(e
                                                                      .data
                                                                      .picture),
                                                                  fit: BoxFit
                                                                      .cover)),
                                                        ),
                                                        const SizedBox(
                                                          width: 10.0,
                                                        ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(e.data.name),
                                                            const SizedBox(
                                                              height: 5.0,
                                                            ),
                                                            Text(
                                                              'Rp.${e.data.price}/porsi',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                          .blue[
                                                                      900],
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                      width: 20.0,
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text(
                                                            "x${e.qty.toString()}"),
                                                        const SizedBox(
                                                          width: 10.0,
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                          const SizedBox(
                                            height: 16.0,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5.0, vertical: 16),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                const Text('Total'),
                                                const Text(':'),
                                                Text(
                                                    'Rp.${cart.fold(0, (previousValue, element) => previousValue + (int.parse(element.data.price ?? '0') * element.qty!))}')
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5.0, vertical: 16),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                const Text('Uang di bayar'),
                                                const Text(':'),
                                                Expanded(
                                                    child: Container(
                                                  width: 150,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8.0),
                                                  child: TextFormField(
                                                    controller: _controller,
                                                    validator: (value) {
                                                      if (int.parse(value!) <
                                                          cart.fold(
                                                              0,
                                                              (previousValue,
                                                                      element) =>
                                                                  previousValue +
                                                                  (int.parse(element
                                                                              .data
                                                                              .price ??
                                                                          '0') *
                                                                      element
                                                                          .qty!) -
                                                                  kembalian)) {
                                                        return 'Uang kurang';
                                                      }
                                                      return null;
                                                    },
                                                    onChanged: (value) {
                                                      if (value == '') {
                                                        kembalian = 0;
                                                      } else {
                                                        kembalian =
                                                            int.parse(value);
                                                      }

                                                      setState(() {});
                                                    },
                                                  ),
                                                ))
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5.0, vertical: 16),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                const Text('Kembalian'),
                                                const Text(':'),
                                                kembalian == 0
                                                    ? const Text('Rp.0')
                                                    : Text(
                                                        'Rp.${cart.fold(0, (previousValue, element) => kembalian - (previousValue + (int.parse(element.data.price ?? '0') * element.qty!)))}')
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: double.infinity,
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 16.0),
                                            child: ElevatedButton(
                                                onPressed: () {},
                                                style: ElevatedButton.styleFrom(
                                                    backgroundColor:
                                                        Colors.blue[900]),
                                                child:
                                                    const Text('Cetak Struk')),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            });
                          });
                    },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue[900]),
                    child: const Text('Charge')),
              )
            ],
          ),
          Visibility(
            visible: showCart,
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: cart.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: 60,
                              height: 60,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          cart[index].data.picture),
                                      fit: BoxFit.cover)),
                            ),
                            const SizedBox(
                              width: 10.0,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(cart[index].data.name),
                                const SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  'Rp.${cart[index].data.price}',
                                  style: TextStyle(
                                      color: Colors.blue[900],
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                if (cart[index].qty == 0) {
                                  cart.removeAt(index);
                                  setState(() {});
                                } else {
                                  cart[index].qty = cart[index].qty! - 1;
                                  setState(() {});
                                }
                              },
                              child: Container(
                                width: 40,
                                // height: 20,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                        width: 2, color: Colors.blue)),
                                child: const Center(child: Icon(Icons.remove)),
                              ),
                            ),
                            const SizedBox(
                              width: 10.0,
                            ),
                            Text(cart[index].qty.toString()),
                            const SizedBox(
                              width: 10.0,
                            ),
                            GestureDetector(
                              onTap: () {
                                cart[index].qty = cart[index].qty! + 1;
                                setState(() {});
                              },
                              child: Container(
                                width: 40,
                                // height: 20,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Colors.blue[900],
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                        width: 2, color: Colors.blue)),
                                child: const Center(
                                    child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                )),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}

class Cart {
  ResponseProduk data;
  int? qty;
  Cart({required this.data, this.qty});
}
