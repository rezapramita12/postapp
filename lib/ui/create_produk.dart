// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reza_mobileappps/bloc/bloc/create_produk/create_produk_bloc.dart';
import 'package:reza_mobileappps/bloc/bloc/produk_bloc.dart';
import 'package:reza_mobileappps/model/response_produk.dart';

class CreateProdukPage extends StatefulWidget {
  const CreateProdukPage({Key? key}) : super(key: key);

  @override
  State<CreateProdukPage> createState() => _CreateProdukPageState();
}

class _CreateProdukPageState extends State<CreateProdukPage> {
  final CreateProdukBloc _createProdukBloc = CreateProdukBloc();
  final ProdukBloc _produkBloc = ProdukBloc();
  List<Cart> cart = [];
  final TextEditingController _foodCode = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _price = TextEditingController();
  final TextEditingController _pictureLink = TextEditingController();
  final TextEditingController _pictureOri = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isEdit = false;
  int? id;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _produkBloc.add(GetProdukEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
        actions: const [],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                // padding: const EdgeInsets.all(12),
                margin: const EdgeInsets.only(),
                child: TextFormField(
                  controller: _foodCode,
                  decoration: const InputDecoration(
                    labelText: 'Food Code',
                    labelStyle: TextStyle(
                      color: Colors.blueGrey,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blueGrey,
                      ),
                    ),
                    // helperText: "What's your name?",
                  ),
                  onChanged: (value) {},
                ),
              ),
              Container(
                // padding: const EdgeInsets.all(12),
                margin: const EdgeInsets.only(),
                child: TextFormField(
                  controller: _name,
                  decoration: const InputDecoration(
                    labelText: 'Name',
                    labelStyle: TextStyle(
                      color: Colors.blueGrey,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blueGrey,
                      ),
                    ),
                    // helperText: "What's your name?",
                  ),
                  onChanged: (value) {},
                ),
              ),
              Container(
                // padding: const EdgeInsets.all(12),
                margin: const EdgeInsets.only(),
                child: TextFormField(
                  controller: _price,
                  decoration: const InputDecoration(
                    labelText: 'Price',
                    labelStyle: TextStyle(
                      color: Colors.blueGrey,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blueGrey,
                      ),
                    ),
                    // helperText: "What's your name?",
                  ),
                  onChanged: (value) {},
                ),
              ),
              Container(
                // padding: const EdgeInsets.all(12),
                margin: const EdgeInsets.only(),
                child: TextFormField(
                  controller: _pictureLink,
                  decoration: const InputDecoration(
                    labelText: 'Picture link',
                    labelStyle: TextStyle(
                      color: Colors.blueGrey,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blueGrey,
                      ),
                    ),
                    // helperText: "What's your name?",
                  ),
                  onChanged: (value) {},
                ),
              ),
              Container(
                // padding: const EdgeInsets.all(12),
                margin: const EdgeInsets.only(),
                child: TextFormField(
                  controller: _pictureOri,
                  decoration: const InputDecoration(
                    labelText: 'Picture ori link',
                    labelStyle: TextStyle(
                      color: Colors.blueGrey,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blueGrey,
                      ),
                    ),
                    // helperText: "What's your name?",
                  ),
                  onChanged: (value) {},
                ),
              ),
              BlocConsumer<CreateProdukBloc, CreateProdukState>(
                bloc: _createProdukBloc,
                listener: (context, state) {
                  if (state is CreateProdukSuccess) {
                    log('success');
                    _produkBloc.add(GetProdukEvent());
                    _foodCode.clear();
                    _name.clear();
                    _pictureLink.clear();
                    _pictureOri.clear();
                    _price.clear();
                  }
                  if (state is EditProdukSuccess) {
                    log('success edit');
                    _produkBloc.add(GetProdukEvent());
                    _foodCode.clear();
                    _name.clear();
                    _pictureLink.clear();
                    _pictureOri.clear();
                    _price.clear();
                  }
                  // TODO: implement listener
                },
                builder: (context, state) {
                  if (state is EditProdukLoading ||
                      state is CreateProdukLoading) {
                    return SizedBox(
                      width: double.infinity * 0.9,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.blue[900],
                        ),
                        onPressed: () {},
                        child: const Center(
                          child: CircularProgressIndicator(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    );
                  }
                  return SizedBox(
                    width: double.infinity * 0.9,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue[900],
                      ),
                      onPressed: () {
                        log('masuk sana');
                        if (isEdit) {
                          _createProdukBloc.add(UpdateProdukEvent(
                              id: id!,
                              foodCode: _foodCode.text,
                              name: _name.text,
                              pictureLink: _pictureLink.text,
                              pictureOriLink: _pictureOri.text,
                              price: _price.text));
                        } else {
                          _createProdukBloc.add(AddProdukEvent(
                              foodCode: _foodCode.text,
                              name: _name.text,
                              pictureLink: _pictureLink.text,
                              pictureOriLink: _pictureOri.text,
                              price: _price.text));
                        }
                        // _createProdukBloc.add(AddProdukEvent(
                        //     foodCode: _foodCode.text,
                        //     name: _name.text,
                        //     pictureLink: _pictureLink.text,
                        //     pictureOriLink: _pictureOri.text,
                        //     price: double.parse(_price.text)));
                      },
                      child: const Text("Kirim"),
                    ),
                  );
                },
              ),
              const SizedBox(
                height: 30.0,
              ),
              BlocConsumer<ProdukBloc, ProdukState>(
                bloc: _produkBloc,
                listener: (context, state) {
                  // TODO: implement listener
                },
                builder: (context, state) {
                  if (state is GetProdukLoading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is GetProdukSuccess) {
                    return GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 20,
                          crossAxisSpacing: 6,
                          childAspectRatio: MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 1.3)),
                      itemCount: state.data.length,
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        ResponseProduk data = state.data[index];
                        return Card(
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  height: 150,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.2),
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16)),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16)),
                                    child: CachedNetworkImage(
                                      imageUrl: data.picture,
                                      placeholder: (context, url) =>
                                          const Center(
                                              child:
                                                  CircularProgressIndicator()),
                                      // errorWidget: (context, url, error) =>
                                      //     SizedBox(
                                      //   height: 200,
                                      //   child: Image.asset(
                                      //     'assets/postingan.png',
                                      //     fit: BoxFit.cover,
                                      //     width: double.maxFinite,
                                      //   ),
                                      // ),
                                      fit: BoxFit.cover,
                                      width: double.maxFinite,
                                    ),
                                  )),
                              const SizedBox(
                                height: 16.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(
                                  data.name,
                                  style: const TextStyle(fontSize: 12),
                                ),
                              ),
                              const SizedBox(
                                height: 3.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(
                                  "Rp.${data.price}/porsi",
                                  style: const TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue),
                                ),
                              ),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  SizedBox(
                                    width: 70,
                                    // padding: const EdgeInsets.symmetric(
                                    //     horizontal: 16.0),
                                    child: ElevatedButton(
                                        onPressed: () {
                                          _foodCode.text = data.foodCode!;
                                          _name.text = data.name;
                                          _pictureLink.text = data.picture;
                                          _pictureOri.text = data.pictureOri;
                                          _price.text = data.price!;
                                          id = data.id;
                                          isEdit = true;
                                          setState(() {});
                                        },
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor: Colors.blue[900]),
                                        child: const Text(
                                          'Edit',
                                          style: TextStyle(fontSize: 10),
                                        )),
                                  ),
                                  BlocConsumer<CreateProdukBloc,
                                      CreateProdukState>(
                                    bloc: _createProdukBloc,
                                    listener: (context, state) {
                                      if (state is DeleteProdukSuccess) {
                                        log('delete berhasil');
                                        _produkBloc.add(GetProdukEvent());
                                      }
                                      // TODO: implement listener
                                    },
                                    builder: (context, state) {
                                      if (state is DeleteProdukLoading) {
                                        return const Text('loading');
                                        // return SizedBox(
                                        //   width: 70,
                                        //   child: ElevatedButton(
                                        //     style: ElevatedButton.styleFrom(
                                        //       backgroundColor: Colors.blue[900],
                                        //     ),
                                        //     onPressed: () {},
                                        //     child: const Center(
                                        //       child: CircularProgressIndicator(
                                        //         color: Colors.white,
                                        //       ),
                                        //     ),
                                        //   ),
                                        // );
                                      } else {
                                        return SizedBox(
                                          width: 70,
                                          // padding: const EdgeInsets.symmetric(
                                          //     horizontal: 16.0),
                                          child: ElevatedButton(
                                              onPressed: () {
                                                showAlertDialogTwoButton(
                                                    context: context,
                                                    onTap: () {
                                                      _createProdukBloc.add(
                                                          DeleteProdukEvent(
                                                              data.id));
                                                      Future.delayed(
                                                          const Duration(
                                                              milliseconds: 5));
                                                      Navigator.pop(context);
                                                    });
                                              },
                                              style: ElevatedButton.styleFrom(
                                                  backgroundColor:
                                                      Colors.blue[900]),
                                              child: const Text(
                                                'Hapus',
                                                style: TextStyle(fontSize: 10),
                                              )),
                                        );
                                      }
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

class Cart {
  ResponseProduk data;
  int? qty;
  Cart({required this.data, this.qty});
}

showAlertDialogTwoButton({BuildContext? context, Function()? onTap}) {
  // set up the buttons
  Widget cancelButton = TextButton(
    onPressed: () {
      Navigator.pop(context!);
    },
    child: const Text('Tidak'),
  );
  Widget continueButton = TextButton(
    onPressed: onTap,
    child: const Text('Ok'),
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
    title: Column(
      // ignore: prefer_const_literals_to_create_immutables
      children: [
        // Image.asset('assets/images/ic_verified.png'),
        const SizedBox(
          height: 15,
        ),
        const Text(
          'Hapus Menu',
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ],
    ),
    content: const Text(
      'Apakah anda yakin akan menghapus Menu ini?',
      textAlign: TextAlign.center,
    ),
    actions: [
      SizedBox(
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                margin: const EdgeInsets.only(left: 16, right: 16, bottom: 10),
                width: 120,
                child: cancelButton),
            Container(
                margin: const EdgeInsets.only(right: 16, bottom: 10),
                width: 120,
                child: continueButton),
          ],
        ),
      ),
    ],
  );

  // show the dialog
  showDialog(
    context: context!,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
